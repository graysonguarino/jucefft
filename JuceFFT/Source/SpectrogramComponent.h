/*
  ==============================================================================

    SpectrogramComponent.h
    Created: 14 May 2021 3:42:23pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

//==============================================================================
/*
*/
class SpectrogramComponent  : public juce::AudioAppComponent,
                            private juce::Timer
{
public:
    SpectrogramComponent();
    ~SpectrogramComponent() override;

    void paint (juce::Graphics&) override;
    // I believe resized() will go in Main.cpp
    //  void resized() override;
    
    void prepareToPlay (int, double) override;
    
    void releaseResources() override;
    
    void getNextAudioBlock (const juce::AudioSourceChannelInfo& bufferToFill) override;
    
    void timerCallback() override;
    
    void pushNextSampleIntoFifo (float sample) noexcept; // TODO - learn about noexcept
    
    void drawNextLineOfSpectrogram();
    
    static constexpr auto fftOrder = 10; // Designates size of FFT window and # of points. Goes by 2 to the power of order
    static constexpr auto fftSize = 1 << fftOrder; // Used in calculating FFT size, 1024 in binary

private:
    juce::dsp::FFT forwardFFT; // Declaring FFT object
    juce::Image spectrogramImage;
    
    std::array<float, fftSize> fifo; // FIFO to hold float array of audio sample data
    std::array<float, fftSize * 2> fftData; // Contains FFT calculations
    int fifoIndex = 0; // Amount of samples in fifo
    bool nextFFTBlockReady = false; // Tells if next FFT block is ready to be rendered
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SpectrogramComponent)
};
