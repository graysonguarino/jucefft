/*
  ==============================================================================

    SpectrogramComponent.cpp
    Created: 14 May 2021 3:42:23pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#include <JuceHeader.h>
#include "SpectrogramComponent.h"

//==============================================================================
SpectrogramComponent::SpectrogramComponent() :
                                                forwardFFT (fftOrder),
                                                spectrogramImage(juce::Image::RGB, 512, 512, true)
{
    setOpaque (true);
    setAudioChannels (2, 0); // 2 input channels, no output
    startTimerHz (60);
    setSize (700, 500);
}

SpectrogramComponent::~SpectrogramComponent()
{
    // This is overridden in the tutorial, not sure why yet.
    shutdownAudio();
}

void SpectrogramComponent::getNextAudioBlock (const juce::AudioSourceChannelInfo& bufferToFill) {
    if (bufferToFill.buffer->getNumChannels() > 0)
    {
        auto* channelData = bufferToFill.buffer->getReadPointer(0, bufferToFill.startSample);
        
        for (auto i = 0; i < bufferToFill.numSamples; ++i) {
            pushNextSampleIntoFifo (channelData[i]);
        }
    }
}

void SpectrogramComponent::paint (juce::Graphics& g)
{
    g.fillAll (juce::Colours::black);
    g.setOpacity(1.0f);
    g.drawImage(spectrogramImage, getLocalBounds().toFloat());
}

//void SpectrogramComponent::resized()
//{
//    // This method is where you should set the bounds of any child
//    // components that your component contains..
//
//}

void SpectrogramComponent::timerCallback() {
    if (nextFFTBlockReady)
    {
        drawNextLineOfSpectrogram();
        nextFFTBlockReady = false;
        repaint();
    }
}

void SpectrogramComponent::pushNextSampleIntoFifo (float sample) noexcept {
    // If fifo contains enough data, set a flag to say that next line should be rendered
    if (fifoIndex == fftSize) { // If fifo full, copy data to fftData array for processing
        if (! nextFFTBlockReady)
        {
            std::fill (fftData.begin(), fftData.end(), 0.0f);
            std::copy (fifo.begin(), fifo.end(), fftData.begin());\
            nextFFTBlockReady = true;
        }
        
        fifoIndex = 0;
    }
    fifo[(size_t) fifoIndex++] = sample; // store new sample in fifo, increment index
    
}

void SpectrogramComponent::drawNextLineOfSpectrogram() {
    auto rightHandEdge = spectrogramImage.getWidth() - 1;
    auto imageHeight = spectrogramImage.getHeight();
    
    // shuffle image left by 1 pixel
    spectrogramImage.moveImageSection(0, 0, 1, 0, rightHandEdge, imageHeight);
    
    // render FFT data
    forwardFFT.performFrequencyOnlyForwardTransform(fftData.data());
    
    // find range of values produced to show detail
    auto maxLevel = juce::FloatVectorOperations::findMinAndMax(fftData.data(), fftSize / 2);
    
    for (auto y = 1; y < imageHeight; ++y)
    {
        auto skewedProportionY = 1.0f - std::exp (std::log ((float) y / (float) imageHeight) * 0.2f);
        auto fftDataIndex = (size_t) juce::jlimit (0, fftSize / 2, (int) (skewedProportionY * fftSize / 2));
        auto level = juce::jmap (fftData[fftDataIndex], 0.0f, juce::jmax (maxLevel.getEnd(), 1e-5f), 0.0f, 1.0f);
        
        spectrogramImage.setPixelAt(rightHandEdge, y, juce::Colour::fromHSV (level, 1.0f, level, 1.0f));
    }
}

void SpectrogramComponent::prepareToPlay (int, double) {}

void SpectrogramComponent::releaseResources() {}

